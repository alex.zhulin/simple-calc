package ru.alexeyzhulin.simplecalc.services;

import com.google.gson.JsonSyntaxException;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.alexeyzhulin.simplecalc.models.CalcResult;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class CalcServiceTest {
    private static CalcService calcService;

    @BeforeClass
    public static void initTests() {
        calcService = new CalcService();
    }

    @Test
    public void testCalculate() {
        final double testValuesDiff[] = {0d, 2d, 10d, -2d};
        final CalcResult expectedResultDiff = new CalcResult(-2d, 10d, 2.5);
        assertEquals("testValuesDiff error", expectedResultDiff, calcService.calculate(testValuesDiff));

        final double testValuesZero[] = {0d, 0d, 0d, 0d};
        final CalcResult expectedResultZero = new CalcResult(0d, 0d, 0d);
        assertEquals("testValuesZero error", expectedResultZero, calcService.calculate(testValuesZero));

        final double testValuesEmpty[] = {};
        final CalcResult expectedResultEmpty = new CalcResult(Double.NaN, Double.NaN, Double.NaN);
        assertEquals("testValuesEmpty error", expectedResultEmpty, calcService.calculate(testValuesEmpty));

    }

    @Test
    public void testTransformStrToArray() {
        final String validArrayString = "[1, 2, 3.4, 0]";
        final double[] expectedValidArray = {1, 2, 3.4, 0};
        assertArrayEquals("testValidArrayString error", expectedValidArray, calcService.transformStrToArray(validArrayString), 0);

        final String emptyArrayString = "[]";
        final double[] expectedEmptyArray = {};
        assertArrayEquals("testEmptyArrayString error", expectedEmptyArray, calcService.transformStrToArray(emptyArrayString), 0);
    }

    @Test(expected = JsonSyntaxException.class)
    public void testTransformException() {
        final String invalidArrayString = "Some invalid string";
        calcService.transformStrToArray(invalidArrayString);
    }
}
