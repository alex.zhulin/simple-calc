package ru.alexeyzhulin.simplecalc.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.alexeyzhulin.simplecalc.models.CalcResult;
import ru.alexeyzhulin.simplecalc.services.CalcService;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/api/application")
@Api(value = "rest-api", description = "Application controller")
public class ApplicationController {

    private static final Logger log = LoggerFactory.getLogger(ApplicationController.class);

    private final CalcService calcService;

    public ApplicationController(CalcService calcService) {
        this.calcService = calcService;
    }

    @ApiOperation(value = "Calc operation", response = CalcResult.class)
    @ApiResponses(value = {
            @ApiResponse(code = HttpServletResponse.SC_OK, message = "Successfully retrieved data"),
            @ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "Internal server error"),
    }
    )
    @RequestMapping(value = "/calc", produces = "application/json", method = RequestMethod.POST)
    public CalcResult calcOperation(HttpServletResponse response,
                                    @RequestBody String arrayAsString) {
        log.info("[calcOperation] Starting");
        log.info("  Request body:");
        log.info("  " + arrayAsString);
        CalcResult calcResult = null;
        try {
            calcResult = calcService.calculate(calcService.transformStrToArray(arrayAsString));
            response.setStatus(HttpServletResponse.SC_OK);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setHeader("Error:", "Request body expected to be a json array of double values. Example [1.0, 2, 5, 0]. Error details: " + e.getMessage());
            log.error("[calcOperation] Error [" + e.getMessage() + "]");
        }
        log.info("[calcOperation] Done");
        return calcResult;
    }
}
