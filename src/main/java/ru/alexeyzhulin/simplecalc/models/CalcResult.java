package ru.alexeyzhulin.simplecalc.models;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Objects;

@ApiModel(description = "Calculation result")
public class CalcResult {
    @ApiModelProperty(notes = "Min value in array")
    private Double minVal;
    @ApiModelProperty(notes = "Max value in array")
    private Double maxVal;
    @ApiModelProperty(notes = "Average value in array")
    private Double avgVal;

    public CalcResult(Double minVal, Double maxVal, Double avgVal) {
        this.minVal = minVal;
        this.maxVal = maxVal;
        this.avgVal = avgVal;
    }

    public Double getMinVal() {
        return minVal;
    }

    public Double getMaxVal() {
        return maxVal;
    }

    public Double getAvgVal() {
        return avgVal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CalcResult that = (CalcResult) o;
        return Objects.equals(minVal, that.minVal) &&
                Objects.equals(maxVal, that.maxVal) &&
                Objects.equals(avgVal, that.avgVal);
    }

    @Override
    public int hashCode() {

        return Objects.hash(minVal, maxVal, avgVal);
    }
}
