package ru.alexeyzhulin.simplecalc.services;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;
import ru.alexeyzhulin.simplecalc.models.CalcResult;

import java.util.Arrays;
import java.util.List;

@Service
public class CalcService {

    /**
     * Function for calculation min, max and average value from array of double values
     *
     * @param inputValues Array of double  values
     * @return Calculation result structure
     */
    public CalcResult calculate(double[] inputValues) {
        Double minValue = Arrays.stream(inputValues)
                .min()
                .orElse(Double.NaN);
        Double maxValue = Arrays.stream(inputValues)
                .max()
                .orElse(Double.NaN);
        Double avgValue = Arrays.stream(inputValues)
                .average()
                .orElse(Double.NaN);
        return new CalcResult(minValue, maxValue, avgValue);
    }

    /**
     * Function for calculation min, max and average value from array of double values
     *
     * @param valuesAsStr Json string of double values
     * @return Array of double values
     */
    public double[] transformStrToArray(String valuesAsStr) {
        List<Double> values = new Gson().fromJson(valuesAsStr, new TypeToken<List<Double>>() {
        }.getType());

        return ArrayUtils.toPrimitive(values.toArray(new Double[0]));
    }
}
